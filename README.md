# Heroes of Pymolie Sale Analysis
This Jupyter Notebook is intended to analyze the sales of game items based on, among other things, player demographics.

## Getting Started

From the project root, navigate to the 'HeroesOfPymoli' directory. There, you will find Jupyter notebook 'HeroesOfPymoli.ipynb'.
The notebook file can be run in Jupyter with following command line statement: 'jupyter-notebook.exe .\HeroesOfPymoli.ipynb'.
Once the notebook is open in Jupyter, select the first cell and press SHIFT + ENTER to run it.
This will also move the cell selection to the next cell down. The remainder of the notebook results
can be viewed by continuing to press SHIFT + ENTER in each cell.

### Prerequisites

This project requires Jupyter Notebook to be installed. Installation instructions can be found [here](https://jupyter.org/install).

### Installing

This project uses only standard library packages, thus a development environment is not necessary.

## Running the tests

Automated tests are included in the Jupyter Notebook file in the form of assert statements. None of the 
assert statements should fail as you execute the notebook.

### Break down into end to end tests

The scripts included in this project are self-contained. The assert statements act as end-to-end tests.

## Deployment

The scripts included in this project are self-contained and intended to be used for one-off runs. Assuming you feel these scripts should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

## Built With

* [Python](http://www.python.org) - Language used
* [Jupyter](https://jupyter.org) - IDE used
* [Pandas](https://pandas.pydata.org) - Data Manipulation Library

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
